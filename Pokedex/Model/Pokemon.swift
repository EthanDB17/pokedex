//
//  Pokemon.swift
//  Pokedex
//
//  Created by Ethan Borrowman on 2/4/19.
//  Copyright © 2019 Ethan Borrowman. All rights reserved.
//

import Foundation

class Pokemon
{
    private var _name: String!
    private var _pokedexId: Int!
    
    var name: String {
        return _name
    }
    
    var pokedexId: Int {
        return _pokedexId
    }
    
    init(name: String, pokedexId: Int) {
        self._name = name
        self._pokedexId = pokedexId
    }
}
