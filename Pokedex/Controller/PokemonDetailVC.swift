//
//  PokemonDetailVC.swift
//  Pokedex
//
//  Created by Ethan Borrowman on 3/3/19.
//  Copyright © 2019 Ethan Borrowman. All rights reserved.
//

import UIKit

class PokemonDetailVC: UIViewController {

    var pokemon: Pokemon!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = pokemon.name
    }

}
